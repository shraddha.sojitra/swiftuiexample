//
//  SpacerLayout.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 07/01/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import SwiftUI

struct SpacerLayout: View {
    var body: some View {
        VStack {
            Image(systemName: "clock")
//            Spacer()
            Divider()
            Text("Time")
        }
    }
}

struct SpacerLayout_Previews: PreviewProvider {
    static var previews: some View {
        SpacerLayout()
    }
}
