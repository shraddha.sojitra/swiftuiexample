//
//  ListView.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 27/12/19.
//  Copyright © 2019 Shraddha Sojitra. All rights reserved.
//

import SwiftUI

//The List container can only loop through an array of items if the type of the array is using the Identifiable protocol
//https://zonneveld.dev/swiftui-list/

struct Student: Identifiable {
    var id = UUID()
    var name: String
}


struct ListView: View {
    
    var students: [Student] = [
        Student(name: "Student 1"),
        Student(name: "Student 2"),
        Student(name: "Student 3")]
    
    var body: some View {
        
        
        NavigationView {
            
            //Static List
            List {
                Text("View 1")
                Text("View 2")
                Text("View 3")
            }
//
            
//            To create static scrollable List
//            List(students) { student in
//                NavigationLink(destination: Text("\(student.name) details")) {
//                    Text(student.name)
//                }
//            }.onAppear {
//                print("Appear")
//            }.onDisappear {
//                print("Disappear")
//            }
            
            
            //To add section
//            List {
//                Section(header: Text("UIKit"), footer: Text("We will miss you")) {
//                    Text("UITableView")
//                }
//
//                Section(header: Text("SwiftUI"), footer: Text("A lot to learn")) {
//                    Text("List")
//                }
//            }
//            .listStyle(GroupedListStyle())
            
            //Perform Edit or Delete
            List {
                ForEach(students, id: \.id) { user in
                    Text(user.name)
                }
                .onDelete(perform: delete)
            }
            .navigationBarItems(trailing: EditButton())

        }
    }
    
    func delete(source: IndexSet) {
        print("deleted")
    }
}

struct ListView_Previews: PreviewProvider {
    static var previews: some View {
        ListView()
    }
}
