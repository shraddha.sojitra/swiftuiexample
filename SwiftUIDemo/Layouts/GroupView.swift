//
//  GroupView.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 04/02/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import SwiftUI

struct GroupView: View {
    var body: some View {
        VStack {
            Group {
                Text("Hello World !")
                Text("Hello World !")
            }.background(Color.pink)
                .padding(10)
            
            Group {
                Text("Hello World !")
                Text("Hello World !")
            }.background(Color.yellow)
            .padding(10)
            
            Group {
                Text("Hello World !")
                Text("Hello World !")
            }.background(Color.green)
            .padding(10)
        }
    }
}

struct GroupView_Previews: PreviewProvider {
    static var previews: some View {
        GroupView()
    }
}
