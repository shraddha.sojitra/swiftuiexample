//
//  FromView.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 07/01/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import SwiftUI

struct FromView: View {
    
    @State var quantity: Int = 0
    @State var selected = ""

    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    Text("Plain Text")
                    Stepper(value: $quantity, in: 0...10, label: { Text("Quantity") })
                }
                Section {
                    Picker(selection: $selected, label:
                        Text("Picker Name")
                        , content: {
                            Text("Value 1").tag(0)
                            Text("Value 2").tag(1)
                            Text("Value 3").tag(2)
                            Text("Value 4").tag(3)
                    })
                }
            }
        }
    }
}

struct FromView_Previews: PreviewProvider {
    static var previews: some View {
        FromView()
    }
}
