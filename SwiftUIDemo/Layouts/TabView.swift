//
//  TabView.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 07/01/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import SwiftUI

struct TabViewDemo: View {
    var body: some View {
        TabView {
            Text("First View")
                .font(.title)
                .tabItem({
                    Image(systemName: "circle")
                    Text("First")
                })
                .tag(0)
            Text("Second View")
                .font(.title)
                .tabItem({
                    Image(systemName: "circle")
                    Text("Second")
                })
                .tag(1)
        }
    }
}

struct TabView_Previews: PreviewProvider {
    static var previews: some View {
        TabViewDemo()
    }
}
 
