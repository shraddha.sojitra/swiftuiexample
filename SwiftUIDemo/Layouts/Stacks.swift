//
//  Stacks.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 27/12/19.
//  Copyright © 2019 Shraddha Sojitra. All rights reserved.
//

import SwiftUI

struct Stacks: View {
    var body: some View {
        ZStack {
            GeometryReader { geometry in
                VStack(spacing: 15) {
                    HStack(spacing: 15) {
                        Text("First View")
                            .foregroundColor(Color.white)
                            .frame(width: geometry.size.width/2, height: geometry.size.height/2)
                            .background(Color.green)
                        Text("Second View")
                            .foregroundColor(Color.white)
                            .frame(width: geometry.size.width/2, height: geometry.size.height/2)
                            .background(Color.blue)
                    }
                    Text("Third View")
                        .foregroundColor(Color.white)
                        .frame(width: geometry.size.width, height: geometry.size.height/2)
                        .background(LinearGradient(gradient: Gradient(colors: [Color.green, Color.blue]), startPoint: .trailing, endPoint: .leading))
                }
            }
        }
        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 0, maxHeight: .infinity)
        .background(Color.gray)
        .foregroundColor(Color.white)
        .edgesIgnoringSafeArea(.all)
    }
}

struct Stacks_Previews: PreviewProvider {
    static var previews: some View {
        Stacks()
    }
}
