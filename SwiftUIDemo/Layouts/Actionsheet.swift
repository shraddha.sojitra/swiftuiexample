//
//  Actionsheet.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 07/01/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import SwiftUI


struct Actionsheet: View {
    
    @State var isSheet: Bool = false

    var actionSheet: ActionSheet {
        ActionSheet(title: Text("Action"),
                    message: Text("Description"),
                    buttons: [
                        .default(Text("OK"), action: {
                            
                        }),
                        .destructive(Text("Delete"), action: {
                            
                        })
                    ]
        )
    }

    var body: some View {
        Button("Action Sheet") {
            self.isSheet = true
        }.actionSheet(isPresented: $isSheet, content: {
            self.actionSheet
        })
    }
}

struct Actionsheet_Previews: PreviewProvider {
    static var previews: some View {
        Actionsheet()
    }
}
