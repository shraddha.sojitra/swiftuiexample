//
//  WebView.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 04/02/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import SwiftUI
import UIKit
import WebKit

struct WebViewPage : UIViewRepresentable {
    func makeUIView(context: Context) -> WKWebView  {
        return WKWebView()
    }
    func updateUIView(_ uiView: WKWebView, context: Context) {
        let req = URLRequest(url: URL(string: "https://www.apple.com")!)
        uiView.load(req)
    }
}

struct WebViewPage_Previews: PreviewProvider {
    static var previews: some View {
        WebViewPage()
    }
}
