//
//  ImagePickerView.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 27/12/19.
//  Copyright © 2019 Shraddha Sojitra. All rights reserved.
//

import Foundation
import SwiftUI

struct ImagePickerView {
  
    @Binding var images: [MyImage]?
    @Binding var isShown: Bool

    func makeCoordinator() -> Coordinator {
      return Coordinator(self)
    }
}

extension ImagePickerView: UIViewControllerRepresentable {
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<ImagePickerView>) -> UIImagePickerController {
        let picker = UIImagePickerController()
        picker.delegate = context.coordinator
        picker.sourceType = .photoLibrary
        return picker
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController,
                                context: UIViewControllerRepresentableContext<ImagePickerView>) {
        
    }
}

class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    var parent: ImagePickerView
    
    init(_ imagePickerController: ImagePickerView) {
        self.parent = imagePickerController
    }

    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let unwrapImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
        parent.images?.append(MyImage(image: unwrapImage))
        parent.isShown = false
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        parent.isShown = false
    }
}
