//
//  ImageListView.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 27/12/19.
//  Copyright © 2019 Shraddha Sojitra. All rights reserved.
//

import SwiftUI
import UIKit

struct MyImage: Identifiable {
    var id = UUID()
    var image: UIImage
}


struct ImageListView: View {
    @State var showActionSheet = false
    @State var selection: Int? = nil
    @State var showImagePicker: Bool = false
    
    @State var images: [MyImage]? = []
    
    init() {
        UITableView.appearance().separatorStyle = .none
    }
    
    var actionSheet: ActionSheet {
        ActionSheet(title: Text("Select From"), message: nil, buttons: [.default(Text("Camera"), action: {
            //            self.showCaptureImageView.toggle()
        }), .default(Text("Photos"), action: {
            print("Photos clicked")
        }), .cancel()])
    }
    
    var body: some View {
        NavigationView {
            
            if showImagePicker {
                ImagePickerView(images: $images, isShown: $showImagePicker)
            } else {
                VStack {
                    Button(action: {
                        self.showImagePicker.toggle()
                    }) {
                        Text("Select Image")
                    }
                    .buttonStyle(GradientBackgroundStyle())
                    
                    List(images!) { imageObj in
                        Image(uiImage: imageObj.image).resizable().scaledToFit()
                    }
                    Spacer()
                }
            }
        }
        .navigationBarTitle("")
        .navigationBarBackButtonHidden(false)
    }
}

struct ImageListView_Previews: PreviewProvider {
    static var previews: some View {
        ImageListView()
    }
}
