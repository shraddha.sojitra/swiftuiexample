//
//  CardView.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 23/01/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import SwiftUI

struct CardView: View {
   let card: Card
       
       var body: some View {
           VStack() {
               HStack() {
                   VStack(alignment: .leading) {
                       Text(card.title)
                           .fixedSize(horizontal: false, vertical: true)
                            .frame(width: 100, height: CGFloat(card.height))
                           .layoutPriority(99)
                   }
                   Spacer()
               }
               .padding([.leading, .trailing, .bottom], 8)
           }
            .background(Color.yellow)
           .cornerRadius(8)
           .background(
               RoundedRectangle(cornerRadius: 8)
                   .stroke(Color.secondary.opacity(0.5))
           )
       }
}

struct Card {
    let title: String
    let subtitle: String
    let height: Int
}
