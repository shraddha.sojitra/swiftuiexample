//
//  CardGridView.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 23/01/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import SwiftUI
import WaterfallGrid

struct CardGridView: View {
    
    @State private var cards: [Card] = Generator.Cards.random()
    
    var body: some View {
        return WaterfallGrid((0..<cards.count), id: \.self) { index in
            CardView(card: self.cards[index])
        }
    }
}


struct Generator {
    
    struct Height {
        static func random() -> [Int] {
            Array(30..<90).map { ($0) }.shuffled()
        }
    }
    
    struct Cards {
        static func random() -> [Card] {
            Height.random().map {
                Card(title: "\($0)", subtitle: "bjashbdahjsdajv", height: $0)
            }
        }
    }
    
}
