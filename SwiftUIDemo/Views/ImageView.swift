//
//  ImageView.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 07/01/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import SwiftUI

struct ImageView: View {
    var body: some View {
//        Image(systemName: "cloud.heavyrain.fill")
//        .foregroundColor(.red)
//        .font(Font.system(.largeTitle).bold())
        
        Image("download")
        .resizable()
        .aspectRatio(contentMode: .fit)

    }
}

struct ImageView_Previews: PreviewProvider {
    static var previews: some View {
        ImageView()
    }
}
