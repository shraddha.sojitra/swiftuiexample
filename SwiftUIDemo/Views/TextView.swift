//
//  ContentView.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 27/12/19.
//  Copyright © 2019 Shraddha Sojitra. All rights reserved.
//

import SwiftUI

//https://www.appcoda.com/learnswiftui/swiftui-text.html
//Text - Font, multiline, rotation effects, shadow

struct TextView: View {
    var body: some View {
       Text("Swift")
           .font(.largeTitle)
//        + Text("is ") .font(.headline)
//        + Text("awesome")
//           .font(.footnote) //You can also concatenate Text together with +.
        .foregroundColor(Color.green)
        .multilineTextAlignment(.center)
        .lineLimit(3)
        .padding(.horizontal, 20)
//        .rotationEffect(.degrees(20), anchor: UnitPoint(x: 0, y: 0))
//        .rotation3DEffect(.degrees(40), axis: (x: 1, y: 0, z: 0))
        .shadow(color: .gray, radius: 2, x: 0, y: 15)

    }
}





//    var body: some View {
//       Text("Swift")
//           .font(.largeTitle)
////        + Text("is ") .font(.headline)
////        + Text("awesome")
////           .font(.footnote) //You can also concatenate Text together with +.
//        .foregroundColor(Color.green)
//        .multilineTextAlignment(.center)
//        .lineLimit(3)
//        .padding(.horizontal, 20)
////        .rotationEffect(.degrees(20), anchor: UnitPoint(x: 0, y: 0))
////        .rotation3DEffect(.degrees(40), axis: (x: 1, y: 0, z: 0))
//        .shadow(color: .gray, radius: 2, x: 0, y: 15)
//
//    }
//}

struct TextView_Previews: PreviewProvider {
    static var previews: some View {
        TextView()
    }
}
