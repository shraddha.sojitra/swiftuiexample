//
//  NavigationLinkView.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 07/01/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import SwiftUI

struct NavigationLinkView: View {
   
    var body: some View {
        NavigationView {
            NavigationLink(destination:
                Text("This is Details")
                .navigationBarTitle(Text("Detail"))
            ) {
                Text("Go to Details screen")
            }.navigationBarTitle(Text(""))
            .navigationBarHidden(true)
        }
    }
}

struct NavigationLinkView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationLinkView()
    }
}
