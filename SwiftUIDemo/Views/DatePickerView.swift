//
//  DatePickerView.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 04/02/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import SwiftUI


var dateFormatter: DateFormatter {
    let formatter = DateFormatter()
    formatter.dateStyle = .long
    return formatter
}

struct DatePickerView: View {
    
    @State private var selectedDate = Date()

    var body: some View {
        VStack {
            Text("Date is \(selectedDate, formatter: dateFormatter)")
            DatePicker(selection: $selectedDate, in: ...Date(), displayedComponents: .date) {
                Text("Select a date")
            }
        }
    }
}

struct DatePickerView_Previews: PreviewProvider {
    static var previews: some View {
        DatePickerView()
    }
}
