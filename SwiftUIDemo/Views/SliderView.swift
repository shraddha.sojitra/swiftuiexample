//
//  SliderView.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 07/01/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import SwiftUI

struct SliderView: View {
    
    @State var progress: Float = 20

    var body: some View {
          Slider(value: $progress, in: 0...100, step: 1)

    }
}

struct SliderView_Previews: PreviewProvider {
    static var previews: some View {
        SliderView()
    }
}
