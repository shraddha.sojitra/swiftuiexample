//
//  PickerView.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 07/01/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import SwiftUI

struct PickerView: View {
   
    var colors = ["Red", "Green", "Yellow", "Blue", "Orange"]
    @State var selectedColor = ""

    var body: some View {
        NavigationView {
           Picker("", selection: self.$selectedColor) {
            ForEach(0..<colors.count) {
                Text(self.colors[$0])
                }
            }
            .labelsHidden()
//           .pickerStyle(SegmentedPickerStyle())
        }
    }
}

struct PickerView_Previews: PreviewProvider {
    static var previews: some View {
        PickerView()
    }
}
