//
//  ContentView.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 27/12/19.
//  Copyright © 2019 Shraddha Sojitra. All rights reserved.
//

import SwiftUI

//https://www.appcoda.com/swiftui-buttons/
//Button - Bgcolor, border, radius

struct ButtonView: View {
    
    var body: some View {
        Button(action: {
         print("Hello, World! Tapped")
        }) {
            HStack{
                Image(systemName: "trash")
                    .font(.title)
                Text("Hello, World!")
            }
        }
        .frame(minWidth: 0, maxWidth: .infinity)
        .padding()
        .background(LinearGradient(gradient: Gradient(colors: [Color.green, Color.blue]), startPoint: .leading, endPoint: .trailing))
        .cornerRadius(40)
        .foregroundColor(Color.white)
        .font(.title)
        .padding(8)
//                .border(Color.green, width: 3)
        .overlay(
            RoundedRectangle(cornerRadius: 40)
                .stroke(Color.green, lineWidth: 5))
        .padding(.horizontal, 20)
    }
}

struct ButtonView_Previews: PreviewProvider {
    static var previews: some View {
        ButtonView()
    }
}
