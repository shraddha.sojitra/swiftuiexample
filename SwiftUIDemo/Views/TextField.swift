//
//  TextField.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 07/01/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import SwiftUI

struct TextFieldView: View {
    
   @State var name: String = "Shraddha"
    
    var body: some View {
        TextField("Name", text: $name)
        .textFieldStyle(RoundedBorderTextFieldStyle())
        .padding()
            .keyboardType(.emailAddress)
    }
}

struct TextField_Previews: PreviewProvider {
    static var previews: some View {
        TextFieldView()
    }
}
