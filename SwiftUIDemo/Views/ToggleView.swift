//
//  ToggleView.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 07/01/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import SwiftUI

struct ToggleView: View {
    
    @State var isShowing = true // toggle state

    var body: some View {
//        Toggle(isOn: $isShowing) {
//            Text("Hello World")
//        }.padding(.horizontal, 20)
        
        Toggle("Hello World", isOn: $isShowing)

    }
}

struct ToggleView_Previews: PreviewProvider {
    static var previews: some View {
        ToggleView()
    }
}
