//
//  SecureField.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 07/01/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import SwiftUI

struct SecureFieldView: View {
    
    @State var password: String = "1234"
   
    var body: some View {
        
        SecureField("Enter Password", text: $password)
            .textFieldStyle(RoundedBorderTextFieldStyle())
            .padding()
    }
    
}

struct SecureField_Previews: PreviewProvider {
    static var previews: some View {
        SecureFieldView()
    }
}
