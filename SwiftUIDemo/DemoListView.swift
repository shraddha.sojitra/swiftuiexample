//
//  DemoListView.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 30/12/19.
//  Copyright © 2019 Shraddha Sojitra. All rights reserved.
//

import SwiftUI
import UIKit

struct DemoListView: View {
    
    var topics = [
        "Views", "Layout", "Flow Example", "UIKit+SwiftUI", "Issues"
    ]
    
    var subTopics = [
        ["Text", "TextField", "SecureField", "Image", "Button", "NavigationLink", "Toggle", "Picker", "Date Picker", "Slider", "Stepper"],
        ["Stack", "List", "ScrollView", "Form", "Group", "Spacer And Devider", "TabView", "ActionSheet"],
        ["Login", "Localization"],
        ["ImagePicker", "WebView", "EmojiTextField", "Charts", "Location", "Grid Layout"],
        ["List animation"]
    ]
    
    
    var body: some View {
        NavigationView {
            List {
                ForEach(topics.indices) { section in
                    Section(header: Text(self.topics[section])) {
                        ForEach(self.subTopics[section].indices) { row in
                            NavigationLink(destination: self.manageNavigation(section: section, index: row)) {
                                Text(self.subTopics[section][row])
                            }
                        }
                        
                    }
                }
            }.navigationBarTitle("SwiftUI")
        }
    }
    
    func manageNavigation(section: Int, index: Int) -> AnyView {
        switch section {
        case 0:
            switch index {
            case 0:
                return AnyView(TextView())
            case 1:
                return AnyView(TextFieldView())
            case 2:
                return AnyView(SecureFieldView())
            case 3:
                return AnyView(ImageView())
            case 4:
                return AnyView(ButtonView())
            case 5:
                return AnyView(NavigationLinkView())
            case 6:
                return AnyView(ToggleView())
            case 7:
                return AnyView(PickerView())
            case 8:
                return AnyView(DatePickerView())
            case 9:
                return AnyView(SliderView())
            case 10:
                return AnyView(StepperView())
            default:
                return AnyView(Text("No view"))
            }
        case 1:
            switch index {
            case 0:
                return AnyView(Stacks())
            case 1:
                return AnyView(ListView())
            case 2:
                return AnyView(ScrollViewDemo())
            case 3:
                return AnyView(FromView())
            case 4:
                return AnyView(GroupView())
            case 5:
                return AnyView(SpacerLayout())
            case 6:
                return AnyView(TabViewDemo())
            case 7:
                return AnyView(Actionsheet())
            default:
                return AnyView(Text("No view"))
            }
        case 2:
            switch index {
            case 0:
                return AnyView(WelcomeView())
            case 1:
                return AnyView(LocalisationView())
            default:
                return AnyView(Text("No view"))
            }
        case 3:
            switch index {
            case 0:
                return AnyView(ImageListView())
            case 1:
                return AnyView(WebViewPage())
            case 2:
                return AnyView(EmojiTF())
            case 3:
                return AnyView(GraphSwiftUI())
            case 4:
                return AnyView(LocationView())
            case 5:
                return AnyView(CardGridView())
            default:
                return AnyView(Text("No view"))
            }
        case 4:
            switch index {
            case 0:
                return AnyView(ListAnimation())
            default:
                return AnyView(Text("No view"))
            }
        default:
            return AnyView(Text("No view"))
        }
    }
}

struct DemoListView_Previews: PreviewProvider {
    static var previews: some View {
        DemoListView()
    }
}
