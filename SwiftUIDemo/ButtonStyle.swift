//
//  ButtonStyle.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 27/12/19.
//  Copyright © 2019 Shraddha Sojitra. All rights reserved.
//

import Foundation
import SwiftUI

struct GradientBackgroundStyle: ButtonStyle {
    
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .padding(8)
            .background(LinearGradient(gradient: Gradient(colors: [Color.green, Color.blue]), startPoint: .leading, endPoint: .trailing))
            .cornerRadius(40)
            .foregroundColor(Color.white)
            .padding(5)
            .overlay(RoundedRectangle(cornerRadius: 40)
                .stroke(Color.green, lineWidth: 3))
            .shadow(color: Color.green, radius: 5, x: 0, y: 0)
            .frame(minWidth: nil, idealWidth: nil, maxWidth: nil, minHeight: nil, idealHeight: 70, maxHeight: 100, alignment: .trailing)
    }
}

struct OnusButtonStyle: ButtonStyle {

    var bgColor: Color
    
    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .frame(minWidth: 0, idealWidth: .infinity, maxWidth: .infinity, minHeight: 50, idealHeight: 50, maxHeight: 50, alignment: .center)
            .padding(0)
            .foregroundColor(.white)
            .background(bgColor)
            .cornerRadius(25)
            .padding(.horizontal, 20)
            .font(.body)
            .scaleEffect(configuration.isPressed ? 0.9 : 1.0)
    }
}
