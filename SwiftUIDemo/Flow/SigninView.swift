//
//  SigninView.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 27/12/19.
//  Copyright © 2019 Shraddha Sojitra. All rights reserved.
//

import SwiftUI

struct LabelTextField : View {
    
    @State var field: Field
    
    var body: some View {
        VStack(alignment: .leading, spacing: 3) {
            Text(field.title)
                .font(.headline)
            TextField(field.placeholder, text: $field.inputStr)
                .padding()
                .background(Color.init(UIColor.lightGray.withAlphaComponent(0.2)))
                .cornerRadius(5)
        }
    }
}


struct SigninView: View {
    
    @State var isError: Bool = false
    @State var errorMessage: String = ""
    
    init() {
        UITableView.appearance().separatorStyle = .none
    }
    
    var dataObj = UserDataSpecifier()
    
    var body: some View {
        
        VStack {
            List(dataObj.fields) { field in
                LabelTextField(field: field)
                //                .padding(.trailing, -25)
            }
            
            NavigationLink(destination: Text("Success")) {
                Text("Submit".uppercased())
                    .frame(minWidth: 0, maxWidth: .infinity)
                    .padding()
                    .alert(isPresented: $isError, content: {
                        Alert(title: Text("Error"), message: Text(self.errorMessage), dismissButton: .default(Text("OK")))
                    })
                    .font(.headline)
                    .foregroundColor(Color.white)
                    .background(Color.green)
                    .cornerRadius(30)
                    .padding(.horizontal, 15)
                    .navigationBarHidden(true)
            }
            
        }
    }
}

struct SigninView_Previews: PreviewProvider {
    static var previews: some View {
        SigninView()
    }
}


class UserDataSpecifier {
    
    var fields: [Field] = []
    
    init() {
        prepareData()
    }
    
    func prepareData() {
        fields.append(Field(placeholder: "Enter Name", title: "Name"))
        fields.append(Field(placeholder: "Enter Email", title: "Email"))
        fields.append(Field(placeholder: "Enter Mobile", title: "Mobile"))
        fields.append(Field(placeholder: "Enter Address", title: "Address"))
    }
    
    func validateData() -> (Bool, String) {
        if fields[0].inputStr.isEmpty {
            return (false, "Please Enter Name")
        } else if fields[1].inputStr.isEmpty {
            return (false, "Please Enter Email")
        } else if fields[2].inputStr.isEmpty {
            return (false, "Please Enter Mobile")
        } else if fields[3].inputStr.isEmpty {
            return (false, "Please Enter Address")
        } else {
            return (true, "")
        }
    }
}

struct Field: Identifiable {
    
    var id = UUID()
    var placeholder: String
    var inputStr: String
    var title: String
    
    init(placeholder: String, inpustStr: String? = "", title: String) {
        self.placeholder = placeholder
        self.inputStr = inpustStr ?? ""
        self.title = title
    }
}
