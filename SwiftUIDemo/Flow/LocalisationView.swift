//
//  LocalisationView.swift
//  SwiftUIDemo
//
//  Created by SHRADDHA on 12/17/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import SwiftUI

struct LocalisationView: View {
   
    let name: LocalizedStringKey = "Shraddha"

    var body: some View {
        //"myNameIs %@" = "My name is %@.";
        Text(name)
    }
}

struct LocalisationView_Previews: PreviewProvider {
    static var previews: some View {
        LocalisationView()
    }
}
