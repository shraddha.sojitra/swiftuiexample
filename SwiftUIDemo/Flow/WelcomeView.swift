//
//  WelcomeView.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 27/12/19.
//  Copyright © 2019 Shraddha Sojitra. All rights reserved.
//

import SwiftUI

struct WelcomeView : View {
    
    @State var selection: Int? = nil
    
    
    var body: some View {
        //            ViewControllerWrapper()
        
            ZStack() {
                Image("LandingBg")
                    .resizable()
                    .scaledToFill()
                    .edgesIgnoringSafeArea(.all)
                VStack(alignment: .center, spacing: 10) {
                    Text("Welcome to")
                        .foregroundColor(Color.white)
                        .font(.body)
                        .padding(.bottom, 50)
                    Image("OnusLogo")
                        .resizable()
                        .scaledToFit()
                        .fixedSize()
                        .padding(.bottom, 10)
                    Text("Free rides for your good times.")
                        .foregroundColor(Color.white)
                        .font(.body)
                        .padding(.bottom, 150)
                        .multilineTextAlignment(.center)
                        .padding(.horizontal, 70)
                    
                    VStack(alignment: .center, spacing: 15) {
                        NavigationLink(destination: SigninView(), tag: 1, selection: $selection) {
                            Button(action: {
                                self.selection = 1
                            }) {
                                Text("Sign In".uppercased())
                            }
                            .buttonStyle(OnusButtonStyle(bgColor: Color.green))
                        }
                        
                        Button(action: {
                        }) {
                            HStack {
                                Text("Continue with facebook".uppercased())
                            }
                        }
                        .buttonStyle(OnusButtonStyle(bgColor: Color(red: 60 / 255, green: 90 / 255, blue: 154 / 255)))
                        
                        Button(action: {
                        }) {
                            HStack {
                                Text("Continue with google".uppercased())
                            }
                        }
                        .buttonStyle(OnusButtonStyle(bgColor: Color(red: 226 / 255, green: 76 / 255, blue: 65 / 255)))
                    }
                }
            }
//        .navigationBarHidden(true)
//        .navigationBarTitle("")
    }
}

