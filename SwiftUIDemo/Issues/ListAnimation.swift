//
//  ListAnimation.swift
//  SwiftUIDemo
//
//  Created by Shraddha Sojitra on 21/01/20.
//  Copyright © 2020 Shraddha Sojitra. All rights reserved.
//

import SwiftUI

struct ListAnimation: View {
    var body: some View {
        List {
            ForEach(1...4, id: \.self) {_ in
                NavigationLink(destination: AView()) {
                    Text("root")
                }
            }
        }
    }
}

struct AView: View {
    var body: some View {
        List {
            ForEach(1...4, id: \.self) {_ in
                NavigationLink(destination: BView()) {
                    Text("aview")
                }
            }
        }
    }
}

struct BView: View {
    var body: some View {
        List {
            
            NavigationLink(destination: BView()) {
                Text("bview")
            }
            NavigationLink(destination: BView()) {
                Text("bview")
            }
            NavigationLink(destination: BView()) {
                Text("bview")
            }
            NavigationLink(destination: BView()) {
                Text("bview")
            }
            NavigationLink(destination: BView()) {
                Text("bview")
            }
            NavigationLink(destination: BView()) {
                Text("bview")
            }
        }
        
        //Solution
//        List {
//            ForEach(1...1000, id: \.self) {_ in
//                NavigationLink(destination: BView()) {
//                    Text("bview")
//                }
//            }
//            .listRowInsets(EdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20))
//        }

    }
}

struct ListAnimation_Previews: PreviewProvider {
    static var previews: some View {
        ListAnimation()
    }
}
