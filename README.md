### Introduction

SwiftUI is a modern way to declare user interfaces for any Apple platform. Create beautiful, dynamic apps faster than ever before.
SwiftUI is an innovative, exceptionally simple way to build user interfaces across all Apple platforms with the power of Swift. Build user interfaces for any Apple device using just one set of tools and APIs.
Apple Inc

Since now, Apple has not provided a preview feature. If we want to see full dynamic UI then we have to run the project and then after we can see it in a simulator. Now we are able to see the immediate result of our UI coding. We can also change UI to dark mode just by selecting an option and it will be reflected instantly. 

In Canvas you can also drag and drop components visually and Xcode will automatically create the SwiftUI code for the component you added to the canvas. So your SwiftUI code and UI are always synchronized. Great!

SwiftUI was also said to save developers time by providing automatic interface layout, automatic support for Dark Mode, automatic support for Accessibility, automatic right-to-left language support and internationalization. Pretty much automatic everything.

### Basic Components
* Text 
* Textfield
* SecureField
* Image
* Button
* NavigationLink
* Toggle
* Picker
* Slider
* Stepper

### Layouts
* Stack
* List
* ScrollView
* Form
* Tab
* Alert
* ActionSheet

### Use UIKit Views with SwiftUI 
* ImagePicker in SwiftUI using UIImagePickerViewController
* WebView in SwiftUI using WebKit
* Charts
* Location Manager
* Grid Layout
* TextField with Emoji Keyboard type (EmojiTextField)

### Reusable views and Styles

### Limitation/Issue in SwiftUI
1. This SDK can only be used starting with iOS 13.
2. No SwiftUI equivalent to UICollectionView
3. Unable to remove or customize list separators
4. Unable to use a UISearchController with a SwiftUI view
5. Unable to know when view becomes active again (equivalent to viewDidAppear)
6. Unable to change status bar colour
7. List doesn't support section index
8. Animation while navigate to other static list only. (https://stackoverflow.com/questions/58505286/how-to-get-rid-off-animation-glitch-when-navigating-to-another-view-in-swiftui)
9. Custom Swipe Actions In List is not available yet
10. Navigation Link only works once in simulator. (https://stackoverflow.com/questions/59279176/navigationlink-works-only-for-once)
11. https://forums.developer.apple.com/thread/126121

### Reference: 
1. https://fuckingswiftui.com/#hstack
2. https://www.hackingwithswift.com/quick-start/swiftui
3. https://github.com/ryangittings/swiftui-bugs
4. https://forums.developer.apple.com/thread/118963
5. https://www.hackingwithswift.com/quick-start/swiftui/what-is-the-published-property-wrapper
6. https://www.hackingwithswift.com/quick-start/swiftui/what-is-the-environmentobject-property-wrapper
7. https://www.hackingwithswift.com/quick-start/swiftui/what-is-the-binding-property-wrapper
8. https://www.hackingwithswift.com/quick-start/swiftui/whats-the-difference-between-observedobject-state-and-environmentobject




